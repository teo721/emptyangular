/**
 * Created by marin on 7/2/2015.
 */
var root = "src/**/",
	dest = "www/";

module.exports = {
	root: root,
	bower: 'bower_components',
	lib: dest + 'lib/',
	dest: dest,
	html: [root + 'www/**/*', '!'+root+'www/**/_*'],
	binJs: [root + "**/.bin/*.js"],
	destJs: dest + 'js/',
	binCss: [root+"**/.bin/*.css"],
	destCss: dest + 'css/'
};