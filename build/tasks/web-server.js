/**
 * Created by marin on 7/2/2015.
 */
var paths = require('./../paths'),
	gulp = require('gulp'),
	webserver = require('gulp-webserver');

gulp.task('webserver', function(){
	return gulp.src('www')
		.pipe(webserver({
			port: 4040,
			livereload: true,
			open: 'http://localhost:4040',
			proxies: [
				
			],
			fallback: 'index.html'
		}));
});