/**
 * Created by marin on 7/2/2015.
 */
var paths = require('./../paths'),
	gulp = require('gulp'),
	mainBowerFiles = require('main-bower-files');

gulp.task('install', function() {
	gulp.src(
		mainBowerFiles({
			includeDev:false
		}),
		{ base: paths.bower }
	)
		.pipe(gulp.dest(paths.lib));
});
