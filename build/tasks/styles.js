/**
 * Created by marin on 7/2/2015.
 */
var paths = require('./../paths'),
	gulp = require('gulp'),
	concat = require('gulp-concat');

gulp.task('styles', function () {
	return gulp.src(paths.binCss, {base: paths.root})
		.pipe(concat('style.css'))
		.pipe(gulp.dest(paths.destCss))
});