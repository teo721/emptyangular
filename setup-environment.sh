sudo apt-get -y update
sudo apt-get -y install python-software-properties python g++ make
sudo apt-get -y install node
sudo apt-get -y install nodejs
sudo apt-get -y install build-essential
sudo apt-get -y install npm
sudo apt-get -y install docker.io
echo 'export PATH=$PATH:/usr/local/bin' >> $HOME/.bashrc
export NODE_PATH=/usr/lib/node_modules
echo $NODE_PATH
