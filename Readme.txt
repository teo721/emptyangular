To get started:
    1. Run npm install in your project root
    2. Run bower install in your project root
    3. gulp install
    4. gulp compile
    5. gulp

If you have any problems contact me on pavelteo721@gmail.com