(function(angular){
	'use strict';

	angular
		.module('Remoty', [
			'ui.router',
			'ngAria',
			'ngAnimate',
			'ngMessages',
			'ngMaterial',
		])
		.config(configure)
		.run(run);

	function configure($locationProvider, $urlRouterProvider, $mdThemingProvider){
		$locationProvider.html5Mode({
			enabled: false,
			rewriteLinks: true
		});
		

		$urlRouterProvider.otherwise("/");


		$mdThemingProvider.theme('default')
			.primaryPalette('cyan')
			.accentPalette('blue')
			.warnPalette('pink')
			.backgroundPalette('grey');

	}

	function run($rootScope, $state, $stateParams ){
		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;
		
	}


}(angular));