/**
 * Created by marin on 7/2/2015.
 */
(function(angular){
	'use strict';

	angular
		.module('code11.domainMapper.test', [

		])
		.config(configure);

	configure.$inject = ['$stateProvider'];

	function configure($stateProvider){
		$stateProvider.state("test", {
			url: "/test",
			templateUrl: "views/test.html",
			controller: "TestCtrl",
			controllerAs: "testCtrl"
		});
	}
}(angular));